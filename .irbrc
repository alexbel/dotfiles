def h
 @_toy_hash ||= {a: 1, b: 2, c: 3}
end

def a
 @_toy_array ||= (1..10).to_a
end

at_exit do
  if IRB.conf[:HISTORY_FILE]
    entries = Readline::HISTORY.entries
    File.write IRB.conf[:HISTORY_FILE], entries.join("\n")
    puts
  end
end

IRB.conf[:SAVE_HISTORY] = 100000


def history(count = 0)
  history_array = Readline::HISTORY.entries
  count = count > 0 ? count : 0
  if count > 0
    from = history_array.length - count
    history_array = history_array[from..-1]
  end
  print history_array.join("\n")
end


def rails_env
  return ' ' if !defined?(Rails)
  envs = Object::HashWithIndifferentAccess.new({
    development: 'dev',
    test: 'test',
    staging: "\001\e[31mstage\e[0m\002",
    production: "\001\e[31mprod\e[0m\002"
  })
  if Rails.version.to_f > 5.2
    "(#{Rails.application.class.module_parent_name}:#{envs[Rails.env]}) "
  else
    "(#{Rails.application.class.parent_name}:#{envs[Rails.env]}) "
  end
end

IRB.conf[:USE_SINGLELINE] = true

IRB.conf[:PROMPT][:CUSTOM] = {
  :PROMPT_I => "#{rails_env}:%03n > ",
  :PROMPT_S => "%03n%l> ",
  :PROMPT_C => "%03n > ",
  :PROMPT_N => "%03n?> ",
  :RETURN => " => %s \n"
}

IRB.conf[:PROMPT_MODE] = :CUSTOM
IRB.conf[:USE_AUTOCOMPLETE] = false
