#!/bin/sh
cd $(dirname $0)
for dotfile in $(ls -A); do
  case $dotfile in
    ..)
      continue;;
    .git)
      continue;;
    .bundle_config)
      continue;;
    README.md)
      continue;;
    setup.sh)
      continue;;
    *)
      ln -Fis "$PWD/$dotfile" $HOME;;
  esac
done

# create simlinks
ln -s ~/.dotfiles/scripts/rails_logs.desktop ~/.config/autostart/rails_logs.desktop

# create bundle config
mkdir ~/.bundle && ln -s ~/.dotfiles/.bundle_config ~/.bundle/config
