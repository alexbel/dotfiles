test -z "$PROFILEREAD" && . /etc/profile || true

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
PATH=$PATH:$HOME/bin

eval $(ssh-agent -s)

export PATH="$HOME/.poetry/bin:$PATH"
