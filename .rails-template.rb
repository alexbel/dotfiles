gem_group :production, :staging do
  gem 'unicorn', '~> 6.1.0'
end

gem_group :development, :test do
  gem 'rspec-rails', '~> 5.1.1'
  gem 'faker', '~> 2.20.0'
  gem 'bullet'
end

gem_group :development do
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'annotate'
end

gem_group :test do
  gem 'simplecov', require: false
end

run 'touch readme.md'
