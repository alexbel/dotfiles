if [ -f ~/.bash-sensible/sensible.bash ]; then
  source ~/.bash-sensible/sensible.bash
fi

export EDITOR=/usr/bin/vim
export TERM='xterm-256color'
# spring temp folder
export SPRING_TMP_PATH=~/tmp
export PATH="$HOME/bin:$PATH"

test -s ~/.alias && . ~/.alias || true

[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm" #this loads rvm

shopt -s no_empty_cmd_completion # don't try to complete empty cmds
shopt -s autocd # change directory without typing cd

if [ -f ~/.liquidprompt/liquidprompt ]; then
  source ~/.liquidprompt/liquidpromptrc-dist
  source ~/.liquidprompt/liquidprompt
fi

if [ -f ~/.git-completion.bash ]; then
  source ~/.git-completion.bash
fi

vman() {
  vim -c "SuperMan $*"

  if [ "$?" != "0" ]; then
    echo "No manual entry for $*"
  fi
}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"
fi

# ruby 2.7.0 silence deprecation warnings
#export RUBYOPT='-W:no-deprecated -W:no-experimental'

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
