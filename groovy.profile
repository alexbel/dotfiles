[Appearance]
BoldIntense=false
ColorScheme=Gruvbox_dark
Font=Source Code Pro Medium,12,-1,5,57,0,0,0,0,0,Regular
UseFontLineChararacters=false

[General]
Name=groovy
Parent=FALLBACK/

[Interaction Options]
ColorFilterEnabled=false
CopyTextAsHTML=false
MouseWheelZoomEnabled=false
UnderlineLinksEnabled=false

[Scrolling]
HistorySize=99999
ScrollBarPosition=1

[Terminal Features]
BlinkingTextEnabled=false
